/* C Library that does the heavy lifting for the JNI project
 *
 *     Copyright (C) 2016  Louis Kemp
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */

#include "libjnixml2.h"
#include <jni.h>
#include <libxml2/libxml/tree.h>
#include <libxml2/libxml/parser.h>
#include <libxml2/libxml/xmlerror.h>
#include <json-c/json.h>

// Global error state
int ErrorOccured = 0;

void xmlPerror(void *context, const char * message){
	// return true if called
	ErrorOccured = 1;
}

json_object * get_element_names(xmlNode * a_node){
	// Iterate through the nodes and generate a json object to return
	// json new json node
	json_object * json_return = json_object_new_object();
	json_object * json_sib_array = json_object_new_array();
	// XML Nodes
	xmlNode * cur_node = NULL;
	//xmlNode * sib_node = NULL;
	xmlChar * node_content = NULL;
	xmlChar * node_name = NULL;
	cur_node = a_node;
	json_bool skip_increment = FALSE;
	while (cur_node != NULL){
		skip_increment = FALSE;
		node_name = (xmlChar *)cur_node->name;
		if (cur_node->type==XML_ELEMENT_NODE){
			// Get the child nodes
			if (xmlNextElementSibling(cur_node) != NULL && xmlChildElementCount(cur_node) > 0){
				// This Element has siblings and Children. Create an array to hold them all.
				while (cur_node!=NULL){
					if (node_name == (xmlChar*) cur_node->name){
						//part of the sibling chain add
						json_object_array_add(json_sib_array, get_element_names(cur_node->children));
						// get the next sibling
						cur_node = xmlNextElementSibling(cur_node);
					}else{
#ifdef _JNI_DEBUG
						// DEBUG CODE
						fprintf(stdout,"broke out because %s <> %s\n", (char *) node_name, (char *) cur_node->name);
						fflush(stdout);
#endif
						// check if this isn't also a sibling node.
						if (cur_node != NULL){
							// it is skip the node increment
							skip_increment = TRUE;
						}
						//not part of the sibling chain
						break;
					}
				}
				// we're done with this pair
			}else if (xmlChildElementCount(cur_node) > 0){
#ifdef _JNI_DEBUG
				// DEBUG CODE
				fprintf(stdout,"object's parent is %s and it should be %s\n", (char *) cur_node->parent->name, (char *) a_node->parent->name);
				fflush(stdout);
#endif
				//drop down into the lower level
				json_object_object_add( json_return, (char *) node_name, get_element_names(cur_node->children));
			}else{
#ifdef _JNI_DEBUG
				// DEBUG CODE
				fprintf(stdout,"object's parent is %s and it should be %s\n", (char *) cur_node->parent->name, (char *) a_node->parent->name);
				fflush(stdout);
#endif
				// try to get the content of this node
				node_content = xmlNodeGetContent(cur_node);
				// check if there is nothing to add
				if (node_content == NULL){
					// Put in a filler
					node_content = (xmlChar *) "--No Content--";
				}
				// add the element's value to the JSON object
				json_object_object_add( json_return, (char *) node_name, json_object_new_string((char *)node_content));
				xmlFree(node_content);
			}
		}
		// if there is something in the array add it to the object as no object would be added otherwise.
		if (json_object_array_length(json_sib_array)>0){
			json_object_object_add( json_return, (char *) node_name, json_sib_array);
			// empty it out when we're done
			json_sib_array = json_object_new_array();
		}
		//onto the next node
		if (cur_node == NULL){
			break;
		}
		// skip this increment cycle
		if (skip_increment == FALSE){
			cur_node = cur_node->next;
		}
	}

	return json_return;
}


JNIEXPORT jstring JNICALL Java_jnilibxml2_xmlparsefile
  (JNIEnv *env, jobject jobj, jstring jstr_FileName){
	// Define variables
	// error handler
	xmlGenericErrorFunc errorhandler = (xmlGenericErrorFunc) xmlPerror;
#ifdef _JNI_DEBUG
	// DEBUG CODE
	fprintf(stdout,"DEBUG MODE ON!\n");
	fflush(stdout);
#endif
	jstring result;
	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	// new json object
	json_object * json_output = json_object_new_object();
	// init the handler
	initGenericErrorDefaultFunc(&errorhandler);
	//Initialize the xml parser
	LIBXML_TEST_VERSION
	//capture the filename
	char * FileName = (char *) (*env)->GetStringUTFChars( env, jstr_FileName, NULL);
	// Start parsing
	doc = xmlReadFile(FileName, NULL, XML_PARSE_NOBLANKS);
	if (ErrorOccured == 1){
		//Error reading the file
		doc = NULL;
	}
	if (doc == NULL) {
		// something went wrong!!
		json_object_object_add(json_output, FileName, json_object_new_string("ERROR: Unable to parse xml document"));
		result = (*env)->NewStringUTF(env, (char *) json_object_to_json_string(json_output));
	}else{
		root_element = xmlDocGetRootElement(doc);
		// add the root element
		json_object_object_add(json_output, FileName , get_element_names(root_element));
		// release the variable to conserve memory and keep it clean
		(*env)->ReleaseStringUTFChars(env, jstr_FileName, NULL);
		// Convert the json object to a java string and return to java
		result = (*env)->NewStringUTF(env, (char *)json_object_to_json_string(json_output));
	}
	// Restore default error handling
	initGenericErrorDefaultFunc(NULL);
	ErrorOccured = 0;
	//free the document from memory
	xmlFreeDoc(doc);
	// clean any global variables assigned
	xmlCleanupParser();
	// return the final output
	return result;
}
