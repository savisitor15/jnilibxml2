makefile:
# java compiler
JC=javac
JH=javah
JFLAGS=-g
# gcc compiler
GCC = gcc
# and variables
CLASS_PATH=../bin 
#define a virtual path for .class in the bin directory
vpath %.class $(CLASS_PATH)

#Compile debug
debug: jnilibxml2.class libjnixml2.so.debug
	cp json-simple.jar ../bin/
	cp run.sh ../bin/
	chmod +x ../bin/run.sh
	
# compile everything
all: jnilibxml2.class libjnixml2.so
	cp json-simple.jar ../bin/
	cp run.sh ../bin/
	chmod +x ../bin/run.sh

# compile the class
jnilibxml2.class:
	$(JC) $(JFLAGS) -d $(CLASS_PATH) -cp .:json-simple.jar jnilibxml2.java

# generate the C header file	
libjnixml2.h: jnilibxml2.class
	$(JH) -classpath $(CLASS_PATH) -o libjnixml2.h jnilibxml2 
	
# Create the shared library, include links to external libs
libjnixml2.so.debug: libjnixml2.o.debug
	$(GCC) -shared -o ../bin/libjnixml2.so libjnixml2.o `xml2-config --libs` -ljson-c

# Create the shared library, include links to external libs
libjnixml2.so: libjnixml2.o
	$(GCC) -shared -o ../bin/libjnixml2.so libjnixml2.o `xml2-config --libs` -ljson-c

#Create the library object file with some testing code still included
libjnixml2.o.debug: libjnixml2.h
	$(GCC) -c -Wall -std=gnu99 -Werror -D _JNI_DEBUG -I /usr/lib/jvm/default/include -I /usr/include/json-c `xml2-config --cflags` -fpic libjnixml2.c
	
#Create the library object file
libjnixml2.o: libjnixml2.h
	$(GCC) -c -Wall -std=gnu99 -Werror -I /usr/lib/jvm/default/include -I /usr/include/json-c `xml2-config --cflags` -fpic libjnixml2.c  

# clean up
clean:
	rm ../bin/libjnixml2.so ../bin/jnilibxml2.class ../bin/run.sh ../bin/json-simple.jar libjnixml2.o 
