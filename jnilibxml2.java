/* A simple Java JNI program that gets json parsed versions of an XML from a C library
 *
 *     Copyright (C) 2016  Louis Kemp
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>
 *   
 */
// Java version 8 lacks a JSON parser use https://github.com/fangyidong/json-simple
import org.json.simple.*;
import org.json.simple.parser.*;

public class jnilibxml2 {
	static{
		System.loadLibrary("jnixml2");
	}
	// give the parser work
	static private native String xmlparsefile(String filename);
	
	public static void main(String[] args){
		// Create Json parser
		JSONParser parser= new JSONParser();
		// Create string that will hold the native call's return
		String[] LibReturnString = new String[args.length];
		Integer i = 0;
		if (args.length <0){
			System.out.println("Please supply a file to parse in commandline");
		}
		try{
			// allow the user to supply as many xml files as they wish
			for(i=0;i < args.length; i++){
				// run the native call on the file and get the string
				LibReturnString[i]=xmlparsefile(args[i]);
				Object obj = parser.parse(LibReturnString[i]);
				// JSON object is parsed and ready
				JSONObject jsonObject = (JSONObject) obj;
				// Dump it to output to confirm everything is functioning.
				System.out.println(jsonObject);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}